﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Interview.AShopkeeper
{
    internal interface IShopkeeper
    {
        public int Solution(int N, int K, int[] cost, int[] sell);
    }
}
