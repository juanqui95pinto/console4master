﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Interview.AShopkeeper
{
    internal class MaximumProfiter : IShopkeeper
    {
        public int Solution(int N, int K, int[] cost, int[] sell)
        {
            int result = 0;
            int actualK = K;
            int[] costCopy = cost;

            if (IsCorrectDataInputs(N, K, cost, sell))
            {
                List<Item> items = new List<Item>();
                List<int> accesibleItems = FillAccesibleItems(actualK, costCopy);

                while (accesibleItems.Count > 0 && items.Count < N)
                {
                    Item item = BuyItem(accesibleItems, sell, costCopy);

                    if (item.Bought && SellItem(item, sell))
                    {
                        actualK += item.Profit;
                        items.Add(item);
                    }
                    if (items.Count < N)
                    {
                        accesibleItems = FillAccesibleItems(actualK, costCopy);
                    }
                }

                foreach (Item item in items)
                {
                    result += item.Profit;
                }
            }
            else
            {
                return 0;
            }

            return result;
        }

        private Item BuyItem(List<int> accesibleItems, int[] sell, int[] costCopy)
        {
            int idx = SelectBestOptionToBuy(accesibleItems, sell, costCopy);
            Item item = new Item();

            if (idx != -1)
            {
                item.Profit = sell[idx] - costCopy[idx];
                item.Bought = true;
                accesibleItems.Remove(idx);
                costCopy[idx] = -1;
            }

            return item;
        }

        private int SelectBestOptionToBuy(List<int> accesibleItems, int[] sell, int[] costCopy)
        {
            int mayor = int.MinValue;
            int resultIdx = -1;
            foreach (int idx in accesibleItems)
            {
                if (mayor < (sell[idx] - costCopy[idx]) && costCopy[idx] != -1)
                {
                    mayor = (sell[idx] - costCopy[idx]);
                    resultIdx = idx;
                }
            }

            return resultIdx;
        }

        private List<int> FillAccesibleItems(int actualK, int[] costCopy)
        {
            List<int> accesibleItems = new List<int>();

            for (int i = 0; i < costCopy.Length; i++)
            {
                if (actualK >= costCopy[i] && costCopy[i] != -1)
                {
                    accesibleItems.Add(i);
                }
            }

            return accesibleItems;
        }

        private bool IsCorrectDataInputs(int n, int k, int[] cost, int[] sell)
        {
            if (n < 0 || k < 0 || cost.Length != sell.Length )
            {
                Console.WriteLine("error: Input data!");
                return false;
            }
            //if (n < 0)
            //{
            //    Console.WriteLine("error: N should be >= 0");
            //    return false;
            //}
            //if (k < 0)
            //{
            //    Console.WriteLine("error: K should be >= 0");
            //    return false;
            //}
            //if (cost.Length != sell.Length)
            //{
            //    Console.WriteLine("error: array-cost should be equal size to array-sell");
            //    return false;
            //}
            for (int i = 0; i < sell.Length; i++)
            {
                if (sell[i] < cost[i] || sell[i] < 0)
                {
                    Console.WriteLine("error:  every corresponding element from array-sell should be equal or greater than array-cost element, ");
                    return false;
                }
            }
            for (int i = 0; i < cost.Length; i++)
            {
                if (cost[i] == 0 || cost[i] < 0)
                {
                    Console.WriteLine("error: there is nothing free!!");
                    return false;
                }
            }
            return true;
        }

        private bool SellItem(Item item, int[] sell)
        {
            item.Sold = true;
            return item.Sold == true ? true : false;
        }

    }
}
