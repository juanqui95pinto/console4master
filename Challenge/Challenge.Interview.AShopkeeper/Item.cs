﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Interview.AShopkeeper
{
    internal class Item
    {
        public int Cost { get; set; }
        public bool Bought { get; set; }
        public int Sell { get; set; }
        public bool Sold { get; set; }
        public int Profit { get; set; }

    }
}
