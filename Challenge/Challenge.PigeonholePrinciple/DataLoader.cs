﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PigeonholePrinciple
{
    class DataLoader : IDataLoader
    {
        public char[] ReadFromFile()
        {
            string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\EightPigeonExample.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\FivePigeons.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\FourPigeonExample.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\TenPigeons.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\TwelvePigeons.txt");

            int amountOfPigeons = int.Parse(lines[0]);
            string beakLine = lines[1];
            string resp = string.Empty;

            for (int i = 0; i < beakLine.Length; i++)
            {
                if (beakLine[i] == '<' || beakLine[i] == '>')
                {
                    resp +=  beakLine[i];
                }
            }

            return resp.ToArray();
        }
    }
}
