﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PigeonholePrinciple
{
    public class RightPigeonsStrategy : IPigeonsPatternGeneratorStrategy
    {
        public char[] GeneratePigeonsPattern(int size)
        {
            char[] rightHalf = new char[size];
            int half = (size / 2) - 1;
            for (int i = 0; i < rightHalf.Length; i++)
            {
                if (i <= half)
                {
                    rightHalf[i] = '>';
                }
                else
                {
                    rightHalf[i] = '<';
                }
            }

            return rightHalf;
        }
    }
}
