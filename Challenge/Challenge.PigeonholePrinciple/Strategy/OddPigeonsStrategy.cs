﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PigeonholePrinciple
{
    public class OddPigeonsStrategy : IPigeonsPatternGeneratorStrategy
    {
        public char[] GeneratePigeonsPattern(int size)
        {
            char[] oddAlternate = new char[size];

            for (int i = 0; i < size; i++)
            {
                oddAlternate[i] = (i % 2 == 0) ? '>' : '<';
            }

            return oddAlternate;
        }
    }
}
