﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PigeonholePrinciple
{
    public class LeftPigeonsStrategy : IPigeonsPatternGeneratorStrategy
    {
        public char[] GeneratePigeonsPattern(int size)
        {
            char[] leftHalf = new char[size];
            int half = (size / 2) - 1;
            for (int i = 0; i < leftHalf.Length; i++)
            {
                if (i > half)
                {
                    leftHalf[i] = '>';
                }
                else
                {
                    leftHalf[i] = '<';
                }
            }

            return leftHalf;
        }
    }
}
