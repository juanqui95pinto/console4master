﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PigeonholePrinciple
{
    public interface IPigeonsPatternGeneratorStrategy
    {
        public char[] GeneratePigeonsPattern(int size);
    }
}
