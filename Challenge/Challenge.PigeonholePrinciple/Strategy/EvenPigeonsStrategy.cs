﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PigeonholePrinciple
{
    public class EvenPigeonsStrategy : IPigeonsPatternGeneratorStrategy
    {
        public char[] GeneratePigeonsPattern(int size)
        {
            char[] evenAlternate = new char[size];

            for (int i = 0; i < evenAlternate.Length; i++)
            {
                if (i % 2 != 0)
                {
                    evenAlternate[i] = '>';
                }
                else
                {
                    evenAlternate[i] = '<';
                }
            }

            return evenAlternate;
        }
    }
}
