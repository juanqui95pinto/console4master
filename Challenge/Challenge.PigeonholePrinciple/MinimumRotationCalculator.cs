﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PigeonholePrinciple
{
    public class MinimumRotationCalculator : IMinimumRotationCalculator
    {
        IPatternCoincidenceCounter coincidenCounter;
        List<IPigeonsPatternGeneratorStrategy> patternGenerators;

        public MinimumRotationCalculator(IPatternCoincidenceCounter coincidenCounter, List<IPigeonsPatternGeneratorStrategy> patternGenerators)
        {
            this.coincidenCounter = coincidenCounter;
            this.patternGenerators = patternGenerators;
        }

        public int CalclMinimumRotation(char[] input)
        {
            var counters = new int[patternGenerators.Count];

            for (int i = 0; i < patternGenerators.Count; i++)
            {
                counters[i] = coincidenCounter.Count(input, patternGenerators[i].GeneratePigeonsPattern(input.Length));
            }

            var maxCoincidence = counters.Max();
            var result = input.Length - maxCoincidence;

            return result;
        }
    }
}
