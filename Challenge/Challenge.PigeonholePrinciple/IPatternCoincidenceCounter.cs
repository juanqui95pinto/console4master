﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PigeonholePrinciple
{
    public interface IPatternCoincidenceCounter
    {
        int Count(char[] input, char[] pattern);
    }
}
