﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PigeonholePrinciple
{
    public class PigeonsPattern
    {
        private IPigeonsPatternGeneratorStrategy strategy;

        public PigeonsPattern(IPigeonsPatternGeneratorStrategy strategy)
        {
            this.strategy = strategy;
        }

        public char[] GeneratePigeonsPattern(int size, IPigeonsPatternGeneratorStrategy strategy)
        {
            return strategy.GeneratePigeonsPattern(size);
        }
    }
}
