﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PigeonholePrinciple
{
    public class PigeonsPatternsGeneratorFactory
    {
        public List<IPigeonsPatternGeneratorStrategy> Create()
        {
            var generators = new List<IPigeonsPatternGeneratorStrategy>();

            generators.Add(new EvenPigeonsStrategy());
            generators.Add(new OddPigeonsStrategy());
            generators.Add(new LeftPigeonsStrategy());
            generators.Add(new RightPigeonsStrategy());


            return generators;
        }
    }
}
