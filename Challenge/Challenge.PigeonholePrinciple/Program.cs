﻿using System;
using System.IO;
using System.Linq;

namespace Challenge.PigeonholePrinciple
{
    class Program
    {
        static void Main(string[] args)
        {
            //SmallestRotate smallestRotate = new SmallestRotate();
            DataLoader dataLoader = new DataLoader();
            char[] originalSquence = dataLoader.ReadFromFile();

            //smallestRotate.CheckOrientation(originalSquence);

            PigeonsPatternsGeneratorFactory factory = new PigeonsPatternsGeneratorFactory();
            PatternCoincidenceCounter pattern = new PatternCoincidenceCounter();

            MinimumRotationCalculator minimumRotationCalculator = new MinimumRotationCalculator(pattern, factory.Create());
            var result = minimumRotationCalculator.CalclMinimumRotation(originalSquence);
            Console.WriteLine(result);
        }
    }
}
