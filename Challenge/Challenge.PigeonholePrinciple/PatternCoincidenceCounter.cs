﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PigeonholePrinciple
{
    public class PatternCoincidenceCounter : IPatternCoincidenceCounter
    {
        public int Count(char[] input, char[] pattern)
        {
            int result = 0;

            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] == pattern[i])
                {
                    result++;
                }
            }

            return result;
        }
    }
}
