﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PigeonholePrinciple
{
    class SmallestRotate
    {
        public void CheckOrientation(char[] originalSquence)
        {
            if (IsEvenInteger(originalSquence))
            {
                var result = ComparePattern(originalSquence);

                Console.WriteLine(result);
            }
            else
            {
                MessageError();
            }
        }

        private int ComparePattern(char[] originalSquence)
        {
            int size = originalSquence.Length;

            //var evenPigeonsPattern = new PigeonsPattern(new EvenPigeonsStrategy());
            //char[] evenAlternate = evenPigeonsPattern.GeneratePigeonsPattern(size);

            //var oddPigeonsPattern = new PigeonsPattern(new OddPigeonsStrategy());
            //char[] oddAlternate = oddPigeonsPattern.GeneratePigeonsPattern(size);

            //var rightPigeonsPattern = new PigeonsPattern(new RightPigeonsStrategy());
            //char[] rightHalf = rightPigeonsPattern.GeneratePigeonsPattern(size);

            //var leftPigeonsPattern = new PigeonsPattern(new LeftPigeonsStrategy());
            //char[] leftHalf = leftPigeonsPattern.GeneratePigeonsPattern(size);

            int leftHalfCount = 0, rightHalfCount = 0, oddAlternateCount = 0, evenAlternateCount = 0;

            //var pigeonsPattern = new PigeonsPattern();

            //char[] evenAlternate = pigeonsPattern.GeneratePigeonsPattern(size, new EvenPigeonsStrategy());
            //char[] oddAlternate = pigeonsPattern.GeneratePigeonsPattern(size, new OddPigeonsStrategy());
            //char[] rightHalf = pigeonsPattern.GeneratePigeonsPattern(size, new RightPigeonsStrategy());
            //char[] leftHalf = pigeonsPattern.GeneratePigeonsPattern(size, new LeftPigeonsStrategy());

            List<int> counts = new List<int>();    

            //for (int i = 0; i < originalSquence.Length; i++)
            //{
            //    if (originalSquence[i] != leftHalf[i])
            //    {
            //        leftHalfCount++;
            //    }
            //    if (originalSquence[i] != rightHalf[i])
            //    {
            //        rightHalfCount++;
            //    }
            //    if (originalSquence[i] != oddAlternate[i])
            //    {
            //        oddAlternateCount++;
            //    }
            //    if (originalSquence[i] != evenAlternate[i])
            //    {
            //        evenAlternateCount++;
            //    }
            //    Console.WriteLine("o: " + originalSquence[i] + " leftHalf:" + leftHalf[i] + " rightHalf:" + rightHalf[i] + " oddAlter:" + oddAlternate[i] + " evenAlter:" + evenAlternate[i]);
            //}

            counts.Add(leftHalfCount);
            counts.Add(rightHalfCount);
            counts.Add(oddAlternateCount);
            counts.Add(evenAlternateCount);

            return counts.Min();
        }

        private void MessageError()
        {
            Console.WriteLine("The amount of pigeons is not correct!");
        }

        private bool IsEvenInteger(char[] originalSquence)
        {
            if (originalSquence.Length % 2 == 0 && originalSquence.Length  >= 2 &&  originalSquence.Length <= 100)
            {
                return true;
            }

            return false;
        }
    }
}
