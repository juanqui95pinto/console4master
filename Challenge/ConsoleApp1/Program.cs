﻿using Challenge.Labyrinth;
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            DataLoader dataLoader = new DataLoader();
            var labyrinh = dataLoader.ReadFromFile();
            CalculatorWalls calculatorArea = new CalculatorWalls();
            var result = calculatorArea.CaculateArea(labyrinh);
            Console.WriteLine(result);
        }
    }
}
