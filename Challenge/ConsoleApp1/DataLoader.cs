﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Labyrinth
{
    class DataLoader : IDataLoader
    {
        public char[][] ReadFromFile()
        {
            
            string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\NineSize.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\tenNazi.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\TextFile1.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\example-input.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\TenSize864.txt");

            int sizeLabyrinth = int.Parse(lines[0]);

            char[][] labyrinth = new char[sizeLabyrinth][];
            int index = 1;
            for (int i = 0; i < sizeLabyrinth; i++)
            {
                labyrinth[i] = lines[index].ToCharArray();
                index++;
            }

            return labyrinth;
        }
    }
}
