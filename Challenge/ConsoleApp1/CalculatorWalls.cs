﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Labyrinth
{
    class CalculatorWalls : ICalculatorWalls
    {
        public int CaculateArea(char[][] matrix)
        {
            int sizeWall = 9;
            int size = matrix.Length;
            int wall = 0;

            int startX = 0;
            int startY = 0;
            char original = '.';
            char fill = '*';

            FloodFill(matrix, startX, startY, original, fill);

            ShowMatrix(matrix);

            for (int y = 0; y < matrix.Length; y++)
            {
                for (int x = 0; x < matrix[0].Length; x++)
                {
                    if (matrix[y][x] == '#')
                    {
                    }
                    else if (matrix[y][x] == '*')
                    {
                        if (x == 0) wall++;
                        if (y == 0) wall++;
                        if (x == size - 1) wall++;
                        if (y == size - 1) wall++;

                        if (x > 0 && matrix[y][x - 1] == '#') wall++;
                        if (y > 0 && matrix[y - 1][x] == '#') wall++;
                        if (x < size - 1 && matrix[y][x + 1] == '#') wall++;
                        if (y < size - 1 && matrix[y + 1][x] == '#') wall++;
                    }
                }
            }

            wall -= 4;

            return wall * sizeWall;
        }

        public void FloodFill(char[][] matrix, int x, int y, char original, char fill)
        {
            int rows = matrix.Length;
            int cols = matrix[0].Length;
            matrix[rows-1][cols-1] = fill;

            if (x < 0 || x >= rows || y < 0 || y >= cols || matrix[x][y] != original)
                return;

            if (matrix[x][y] == original) matrix[x][y] = fill;

            FloodFill(matrix, x + 1, y, original, fill); // Abajo
            FloodFill(matrix, x - 1, y, original, fill); // Arriba
            FloodFill(matrix, x, y + 1, original, fill); // Derecha
            FloodFill(matrix, x, y - 1, original, fill); // Izquierda
        }

        private void ShowMatrix(char[][] matrix)
        {
            for (int y = 0; y < matrix.Length; y++)
            {
                for (int x = 0; x < matrix[0].Length; x++)
                {
                    Console.Write(matrix[y][x]);
                }
                Console.WriteLine();
            }
        }
    }
}
