﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.NewSquareCountry
{
    class BuiltCertificate : IBuiltCertificate
    {
        public int Calculate(int number)
        {
            List<int> serieSquares = new List<int>();

            serieSquares = FillSerieSquares(number);

            return serieSquares[number];
        }

        private List<int> FillSerieSquares(int number)
        {
            List<int> results = new List<int>();

            for (int index = 0; index <= number; index++)
            {
                if (IsSquarePerfect(index))
                {
                    results.Add(1);
                }
                else if (index > 1)
                {
                    results.Add(SearchOptimus(results, index, number));
                }
            }

            return results;
        }

        private int SearchOptimus(List<int> originals, int idx, int number)
        {
            List<int> temps = new List<int>();

            for (int complemento = 1; complemento < idx; complemento++)
            {
                temps.Add(originals[idx - complemento] + originals[complemento]);
            }
            
            return temps.Min();
        }

        private bool IsSquarePerfect(int num)
        {
            double root = Math.Sqrt(num);
            return root == Math.Ceiling(root);
        }
    }
}
