﻿using System;
using System.Collections.Generic;

namespace Challenge.NewSquareCountry
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleView consoleView = new ConsoleView();
            int number = consoleView.InputData();
            BuiltCertificate builtCertificate = new BuiltCertificate();

            int result = builtCertificate.Calculate(number);

            Console.WriteLine(result);

        }
    }
}
