﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.NewSquareCountry
{
    interface IBuiltCertificate
    {
        public int Calculate(int iData);
    }
}
