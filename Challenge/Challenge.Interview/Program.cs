﻿using System;
using System.Collections.Generic;

namespace Challenge.Interview
{
    class Program
    {
		public static void Main()
		{
			// return index of first instance of first duplicate value
			int[] findDuplicate = new int[] { 5, 2, 3, 6, 7, 3, 2, 8, 8 };
			//--------------------------------------^
			Console.WriteLine(FirstIndexOfFirstDuplicate(findDuplicate)); // Should write 2 since findDuplicate[2] == 3
		}

        public static int FirstIndexOfFirstDuplicate(int[] inputArray)
        {
            var dic = new Dictionary<int, int>();

            for (int i = 0; i < inputArray.Length; i++)
            {
                if (dic.ContainsKey(inputArray[i]))
                {
                    return dic[inputArray[i]];
                }

                dic[inputArray[i]] = i;
            }

            return -1;
        }

        //private static int FirstIndexOfFirstDuplicate(int[] fd)
        //{
        //    List<int> nums = new List<int>();

        //    for (int i = 0; i < fd.Length; i++)
        //    {
        //        if (nums.Contains(fd[i]))
        //        {
        //            return Indexx(i, fd);
        //        }
        //        else
        //        {
        //            nums.Add(fd[i]);
        //        }
        //    }

        //    return 0;
        //}

        //private static int Indexx(int n, int[] fd)
        //{
        //    for (int i = 0; i < fd.Length; i++)
        //    {
        //        if (fd[n] == fd[i])
        //        {
        //            return i;
        //        }
        //    }

        //    return 0;
        //}
    }
}
