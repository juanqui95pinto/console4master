﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PhoneNumbers
{
    public interface INumberTranscription
    {
        public List<string> ShortSequence(List<PhoneNumber> phoneNumbers, Alphabet a);
    }
}
