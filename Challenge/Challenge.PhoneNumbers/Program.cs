﻿using System;
using System.Collections.Generic;

namespace Challenge.PhoneNumbers
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Alphabet abcd = new Alphabet();
            
            DataLoader dataLoader = new DataLoader();
            List<PhoneNumber> phoneNumbers = dataLoader.ReadFromFile();

            NumberTranscription numberTranscription = new NumberTranscription();
            List<PhoneNumber> phoneNumbersDictionary = numberTranscription.FillWordsDictionary(phoneNumbers, abcd);
            List<string> result = numberTranscription.ShortSequence(phoneNumbersDictionary);

            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }

    }
}
