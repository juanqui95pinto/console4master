﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PhoneNumbers
{
    public class Alphabet
    {
        static Dictionary<char,int> letterToNumber = new Dictionary<char, int>
        {
            {'a', 2}, 
            {'b', 2}, 
            {'c', 2},
            {'d', 3}, 
            {'e', 3}, 
            {'f', 3},
            {'g', 4}, 
            {'h', 4},
            {'i', 1}, 
            {'j', 1},
            {'k', 5}, 
            {'l', 5},
            {'m', 6}, 
            {'n', 6},
            {'o', 0},
            {'p', 7}, 
            {'r', 7}, 
            {'s', 7},
            {'t', 8}, 
            {'u', 8}, 
            {'v', 8},
            {'w', 9}, 
            {'x', 9}, 
            {'y', 9},
            {'z', 0},
            {'q', 0}
        };

        public int GetNumberFromLetter(char letter)
        {
            return letterToNumber[letter];
        }

        public void ShowAlphabet()
        {
            foreach (var item in letterToNumber)
            {
                Console.WriteLine(item.Key + " " + item.Value);
            }
        }
    }
}
