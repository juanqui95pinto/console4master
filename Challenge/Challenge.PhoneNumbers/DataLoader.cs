﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PhoneNumbers
{
    public class DataLoader
    {
        public List<PhoneNumber> ReadFromFile()
        {
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\SampleInput.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\Greetings.txt");
            string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\LongInputByMaster.txt");

            int count = 0; //List<string> words = new List<string>();
            List<PhoneNumber> phoneNumbers = new List<PhoneNumber>();
            while (true)
            {
                string callNumber = lines[count]; 
                count++;
                int wordSize = int.Parse(lines[count]); 
                count++;
                List<string> words = new List<string>();

                for (int i = 0; i < wordSize; i++)
                {
                    words.Add(lines[count++]);
                }

                PhoneNumber pn = new PhoneNumber();
                pn.CallNumber = callNumber;
                pn.WordSize = wordSize;
                pn.Words = words;
                phoneNumbers.Add(pn);

                if (BigInteger.TryParse(lines[count], out BigInteger number))
                {
                }
                if(number == -1)
                {
                    break;
                }
            }

            //ShowData(phoneNumbers);

            return phoneNumbers;
        }

        private void ShowData(List<PhoneNumber> phoneNumbers)
        {
            foreach (var item in phoneNumbers)
            {
                Console.WriteLine(item.CallNumber);
                Console.WriteLine(item.WordSize);
                foreach (var element in item.Words)
                {
                    Console.WriteLine(element);
                }
            }
        }
    }
}
