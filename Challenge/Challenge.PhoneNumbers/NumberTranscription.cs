﻿using Challenge.PhoneNumbers.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PhoneNumbers
{
    public class NumberTranscription //: INumberTranscription
    {
        public List<PhoneNumber> FillWordsDictionary(List<PhoneNumber> phoneNumbers, Alphabet a)
        {
            foreach (var item in phoneNumbers)
            {
                Dictionary<string, string> DicWords = new Dictionary<string, string>();
                foreach (var word in item.Words)
                {
                    DicWords[word] = NumbersOfWord(word, a);
                }
                item.DictionaryWords = DicWords;
            }


            foreach (var item in phoneNumbers)
            {
                foreach (KeyValuePair<string, string> kvp in item.DictionaryWords)
                {
                    Console.WriteLine($"word: {kvp.Key}, number: {kvp.Value}");
                }
                Console.WriteLine();
            }


            return phoneNumbers;
        }

        private string NumbersOfWord(string word, Alphabet a)
        {
            string result = string.Empty;

            foreach (char letter in word)
            {
                result += a.GetNumberFromLetter(letter);
            }

            return result;
        }

        public List<string> ShortSequence(List<PhoneNumber> input)
        {
            List<string> wordsResult = new List<string>();
            foreach (var item in input)
            {
                TreeNode node = new TreeNode(item.CallNumber);
                FillTree(node, item);
                PrintTree(node,0);
                var shortestSequesce = GetShortestSequenceOfWords(node);
                Console.WriteLine( string.Join(' ', shortestSequesce.ToArray()));
                Console.WriteLine("==================================");
               // wordsResult = GetPathTree(node);
            }


            return wordsResult;
        }

        private bool FillTree(TreeNode n, PhoneNumber input)
        {
            if (n.Number == null || n.Number == string.Empty)
            {
                return true;
            }
            var coincidences = input.DictionaryWords.Where(x => n.Number.StartsWith(x.Value));

            foreach (var c in coincidences)
            {

                var x = new TreeNode(n.Number.Substring(c.Value.Length), c.Key);
                if(FillTree(x, input))
                {
                    n.Children.Add(x);
                }
            }

            return n.Children.Count > 0;
        }
        private List<string> GetShortestSequenceOfWords(TreeNode n)
        {
            List<string> result = new List<string>();

            if (n.Number == null || n.Number == string.Empty)
            {
                result.Add(n.WordToIdentify);
                return result;
            }

            for (int i = 0; i < n.Children.Count; i++)
            {
                var resChild = GetShortestSequenceOfWords(n.Children[i]);
                if (i == 0)
                {
                    result = resChild;
                }
                else if (resChild.Count < result.Count)
                {
                    result = resChild;
                }
            }

            result.Insert(0, n.WordToIdentify);

            return result;
        }

        //private void FillTree(TreeNode n, PhoneNumber input)
        //{
        //    if (n.Number==null || n.Number==string.Empty)
        //    {
        //        return;
        //    }

        //    var coincidences = input.DictionaryWords.Where(x => n.Number.StartsWith(x.Value));
        //    foreach (var c in coincidences)
        //    {
        //        var x = n.AddChild(n.Number.Substring(c.Value.Length), c.Key);
        //        FillTree(x, input);
        //    }
        //}

        static void PrintTree(TreeNode node, int depth)
        {
            Console.WriteLine($"{new string('-', depth)} {node.WordToIdentify}");

            foreach (var child in node.Children)
            {
                PrintTree(child, depth + 1);
            }
        }

        //static List<string> GetPathTree(TreeNode node)//GetPathTree(node, depth - i);
        //{
        //    List<string> result = new List<string>();

        //    if (node.Number == null || node.Number == string.Empty)
        //    {
        //        return;
        //    }

        //    var coincidences = input.DictionaryWords.Where(x => n.Number.StartsWith(x.Value));
        //    foreach (var c in coincidences)
        //    {
        //        var x = n.AddChild(n.Number.Substring(c.Value.Length), c.Key);
        //        FillTree(x, input);
        //    }

        //    return result;
        //}

    }
}
