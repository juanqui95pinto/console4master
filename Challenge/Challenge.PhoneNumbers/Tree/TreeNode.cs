﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PhoneNumbers.Tree
{
    public class TreeNode
    {
        public string WordToIdentify { get; set; }
        public string Number { get; set; }

        public List<TreeNode> Children { get; set; }

        public TreeNode( string number)
        {
            Number = number;
            Children = new List<TreeNode>();
        }

        public TreeNode(string number, string word):this(number)
        {
            WordToIdentify = word;
        }
    }
}
