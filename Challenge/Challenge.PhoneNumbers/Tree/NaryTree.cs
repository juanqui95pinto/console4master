﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PhoneNumbers.Tree
{
    public class NaryTree
    {
        public TreeNode Root { get; set; }

        public NaryTree(string number)
        {
            Root = new TreeNode(number);
        }
    }
}
