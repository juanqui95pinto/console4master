﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PhoneNumbers
{
    public class PhoneNumber
    {
        public string CallNumber { get; set; }
        public int WordSize { get; set; }
        public List<string> Words { get; set; }
        public Dictionary<string, string> DictionaryWords { get; set; }
    }
}
