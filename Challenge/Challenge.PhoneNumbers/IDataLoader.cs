﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.PhoneNumbers
{
    interface IDataLoader
    {
        List<PhoneNumber> ReadFromFile();
    }
}
