﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Challenge.RestaurantInterview
{
    class Program
    {
        static void Main(string[] args)
        {
            Show show = new Show();
            ReservaMesa reservaMesa = new ReservaMesa();

            var matrixDeMesas = reservaMesa.LlenarMatrixMesas();

            show.Mesas(matrixDeMesas);

            string nombreDeCliente = show.IngresarNombreCliente();

            int cantPersonas = show.IngresarCantidadPersonas();

            var mesaReservada = reservaMesa.ResevarMesa(matrixDeMesas, cantPersonas, nombreDeCliente);

            if (mesaReservada != null)
            {
                mesaReservada.Disponible = false;
                if (mesaReservada.CompuestaPor == null)
                {
                    show.Mesa(mesaReservada, nombreDeCliente);
                }
                else if (mesaReservada.CompuestaPor.Count > 0)
                {
                    show.UnirMesas(mesaReservada, nombreDeCliente);
                }
            }
            else
            {
                show.NoExistenMesasDisponibles();
            }
            show.Mesas(matrixDeMesas);

            Console.ReadKey();
        }

      
    }
}
