﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.RestaurantInterview
{
    public class Mesa
    {
        public string CodMesa { get; set; }
        public string ReservadoPor { get; set; } 
        public int CantPersonas { get; set; }
        public List<Mesa> CompuestaPor { get; set; }
        public bool Disponible { get; set; }
        public string Observacion { get; set; }
        public int PosX { get; set; }
        public int PosY { get; set; }
    }
}
