﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.RestaurantInterview
{
    public class Show
    {
        public void NoExistenMesasDisponibles()
        {
            Console.WriteLine("No Existen Mesas Disponibles!!!");
        }

        public void Mesa(Mesa mesa, string nombreDeCliente)
        {
            Console.WriteLine("la mesa en X=" + mesa.PosX + " en Y=" + mesa.PosY + " esta reservada, para el cliente: " + nombreDeCliente);
        }

        public int IngresarCantidadPersonas()
        {
            Console.Write("Ingresar Cantidad Personas: ");
            return Convert.ToInt32(Console.ReadLine());
        }

        public string IngresarNombreCliente()
        {
            Console.Write("Ingresar Nombre Cliente: ");
            return Console.ReadLine();
        }

        public void UnirMesas(Mesa mesa, string nombreDeCliente)
        {
            Console.WriteLine("===================================");
            Console.WriteLine("lista de mesas reservadas, para el cliente: " + nombreDeCliente);
            Console.WriteLine("-----------------------------------");
            foreach (var item in mesa.CompuestaPor)
            {
                Console.WriteLine("X={0},Y={1} cap:{2} {3}", item.PosX, item.PosY, item.CantPersonas, item.Disponible);
            }
            Console.WriteLine("===================================");
        }

        public void Mesas(Mesa[][] matrixDeMesas)
        {
            for (int y = 0; y < matrixDeMesas.Length; y++)
            {
                for (int x = 0; x < matrixDeMesas[y].Length; x++)
                {
                    Console.Write("[{0}][{1}]", x, y);

                    if (matrixDeMesas[y][x] == null)
                    {
                        Console.Write("= NULL ");
                    }
                    else
                    {
                        Console.Write("= {0}    {1}", matrixDeMesas[y][x].CantPersonas, matrixDeMesas[y][x].Disponible);
                    }
                }
                Console.WriteLine();
            }
        }

        public int IngresarCapacidadMesa()
        {
            Console.Write("(Zero = null) Capacidad mesa = ");
            return Convert.ToInt32(Console.ReadLine());
        }

        public int IngresarTamanioMatriY()
        {
            Console.Write("Ingrese tamanio matrix en Y: ");
            return Convert.ToInt32(Console.ReadLine());
        }

        public int IngresarTamanioMatriX()
        {
            Console.Write("Ingrese tamanio matrix en X: ");
            return Convert.ToInt32(Console.ReadLine());
        }
    }
}
