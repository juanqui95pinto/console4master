﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.RestaurantInterview
{
    public class ReservaMesa
    {
        public Mesa ResevarMesa(Mesa[][] matrixDeMesas, int cantPersonas, string nombreDeCliente)
        {
            List<Mesa> mesasSimples = new List<Mesa>();
            Mesa result = null;

            if (!ExisteMesasDisponible(matrixDeMesas))
            {
                return result;
            }

            for (int y = 0; y < matrixDeMesas.Length; y++)
            {
                for (int x = 0; x < matrixDeMesas[y].Length; x++)
                {
                    if (matrixDeMesas[y][x] != null && matrixDeMesas[y][x].CantPersonas == cantPersonas && matrixDeMesas[y][x].Disponible)
                    {
                        matrixDeMesas[y][x].PosX = x;
                        matrixDeMesas[y][x].PosY = y;
                        return matrixDeMesas[y][x];
                    }
                    else if (matrixDeMesas[y][x] != null && matrixDeMesas[y][x].CantPersonas >= cantPersonas && matrixDeMesas[y][x].Disponible)
                    {
                        mesasSimples.Add(matrixDeMesas[y][x]);
                    }
                }
            }

            if (mesasSimples.Count > 0)
            {
                return EncontrarSimpleMesaMenor(mesasSimples);
            }

            var menorMesa = MenorMesa(matrixDeMesas);

            if (cantPersonas < menorMesa.CantPersonas)
            {
                return menorMesa;
            }

            var mayorMesa = MayorMesa(matrixDeMesas);

            if (cantPersonas > mayorMesa.CantPersonas)
            {
                result = JuntarMesas(matrixDeMesas, cantPersonas);
                if (result == null)
                {
                    return result;
                }
            }
            return result;
        }

        private bool ExisteMesasDisponible(Mesa[][] matrixDeMesas)
        {
            int count = 0;

            for (int y = 0; y < matrixDeMesas.Length; y++)
            {
                for (int x = 0; x < matrixDeMesas[y].Length; x++)
                {
                    if (matrixDeMesas[y][x] == null)
                    {
                        count++;
                    }
                }
            }

            if (count == matrixDeMesas.Length * matrixDeMesas[0].Length)
            {
                return false;
            }

            return true;
        }

        private Mesa EncontrarSimpleMesaMenor(List<Mesa> mesasSimples)
        {
            int menor = int.MaxValue;
            Mesa mesaResult = new Mesa();

            foreach (var item in mesasSimples)
            {
                if (item.CantPersonas < menor)
                {
                    menor = item.CantPersonas;
                    mesaResult = item;
                }
            }

            mesaResult.Disponible = false;

            return mesaResult;
        }

        private Mesa MenorMesa(Mesa[][] matrixDeMesas)
        {
            int menorMesa = int.MaxValue;
            Mesa mesaResult = new Mesa();

            for (int y = 0; y < matrixDeMesas.Length; y++)
            {
                for (int x = 0; x < matrixDeMesas[y].Length; x++)
                {
                    if (matrixDeMesas[y][x] != null && matrixDeMesas[y][x].CantPersonas < menorMesa && matrixDeMesas[y][x].Disponible)
                    {
                        menorMesa = matrixDeMesas[y][x].CantPersonas;
                        mesaResult = matrixDeMesas[y][x];
                    }
                }
            }

            return mesaResult;
        }

        private Mesa MayorMesa(Mesa[][] matrixDeMesas)
        {
            int mayorMesa = int.MinValue;
            Mesa result = new Mesa();

            for (int y = 0; y < matrixDeMesas.Length; y++)
            {
                for (int x = 0; x < matrixDeMesas[y].Length; x++)
                {
                    if (matrixDeMesas[y][x] != null && matrixDeMesas[y][x].CantPersonas > mayorMesa && matrixDeMesas[y][x].Disponible)
                    {
                        mayorMesa = matrixDeMesas[y][x].CantPersonas;
                        result = matrixDeMesas[y][x];
                    }
                }
            }

            return result;
        }

        private Mesa JuntarMesas(Mesa[][] matrixDeMesas, int cantPersonas)
        {
            List<List<Mesa>> mesasResults = new List<List<Mesa>>();

            for (int y = 0; y < matrixDeMesas.Length; y++)
            {
                List<Mesa> listaTemporal = new List<Mesa>();
                int sum = 0;

                for (int x = 0; x < matrixDeMesas[y].Length; x++)
                {
                    if (matrixDeMesas[y][x] != null)
                    {
                        listaTemporal.Add(matrixDeMesas[y][x]);
                        if (cantPersonas <= (sum += matrixDeMesas[y][x].CantPersonas))
                        {
                            mesasResults.Add(listaTemporal);
                        }
                    }
                    else
                    {
                        sum = 0;
                    }
                }
            }

            for (int x = 0; x < matrixDeMesas[0].Length; x++)
            {
                List<Mesa> listaTemporal = new List<Mesa>();
                int sum = 0;

                for (int y = 0; y < matrixDeMesas.Length; y++)
                {
                    if (matrixDeMesas[y][x] != null)
                    {
                        listaTemporal.Add(matrixDeMesas[y][x]);
                        if (cantPersonas <= (sum += matrixDeMesas[y][x].CantPersonas))
                        {
                            mesasResults.Add(listaTemporal);
                        }
                    }
                    else
                    {
                        sum = 0;
                    }
                }
            }

            Mesa mesa = EncontrarMesaConMenor(mesasResults);

            return mesa.CompuestaPor.Count > 0 ? mesa : null;
        }

        private Mesa EncontrarMesaConMenor(List<List<Mesa>> mesasResults)
        {
            Mesa mesaResult = new Mesa();
            int mesasMenores = int.MaxValue;
            int sillasMenores = int.MaxValue;
            int sum = 0;
            int index = 0;

            List<List<Mesa>> mesasAuxiliares = new List<List<Mesa>>();

            foreach (var items in mesasResults)
            {
                if (items.Count <= mesasMenores)
                {
                    mesasMenores = items.Count;
                    mesasAuxiliares.Add(items);
                }
            }

            foreach (var mesas in mesasAuxiliares)
            {
                foreach (var mesa in mesas)
                {
                    sum += mesa.CantPersonas;
                }
                if (sum < sillasMenores)
                {
                    index = mesasAuxiliares.IndexOf(mesas);
                }
            }

            foreach (var item in mesasAuxiliares[index])
            {
                item.Disponible = false;
            }

            mesaResult.CompuestaPor = mesasAuxiliares[index];

            return mesaResult;
        }

        public Mesa[][] LlenarMatrixMesas()
        {
            Show show = new Show();

            int n = show.IngresarTamanioMatriX();
            int m = show.IngresarTamanioMatriY(); ;
            Mesa[][] matrixDeMesas = new Mesa[n][];
            int capacidadMesa = 0;

            for (int y = 0; y < n; y++)
            {
                matrixDeMesas[y] = new Mesa[m];
                for (int x = 0; x < m; x++)
                {
                    capacidadMesa = show.IngresarCapacidadMesa();

                    if (capacidadMesa == 0)
                    {
                        matrixDeMesas[y][x] = null;
                    }
                    else
                    {
                        Mesa mesa = new Mesa();
                        mesa.CantPersonas = capacidadMesa;
                        mesa.Disponible = true;
                        mesa.PosX = x;
                        mesa.PosY = y;
                        matrixDeMesas[y][x] = mesa;
                    }
                }
            }

            return matrixDeMesas;
        }
    }
}
