﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.TheTimeTakeStones
{
    internal interface IStoneGameSimulator
    {
        public int PlaySimulation(InputData inData);
    }
}
