﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.TheTimeTakeStones
{
    internal class ConsoleView
    {
        public void ShowInputData(InputData id)
        {
            Console.WriteLine(id.Stones);

            foreach (var item in id.TypeTakes)
            {
                Console.Write(item+" ");
            }
        }

        public void MessageError()
        {
            Console.WriteLine("Number out of range!!");
        }
    }
}
