﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.TheTimeTakeStones
{
    internal class DataLoader : IDataLoader
    {
        public InputData ReadFromFile()
        {
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\NewLittleCase.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\NewOneHundredStones.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\NewEightTakeStones.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\NewInterestingCase.txt");
            string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\NewLargeCase.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\MasterSecondExample.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\MasterExample.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\PerfectGame - Copy.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\newCase.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\PerfectGame.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\OriginalExample.txt");


            InputData id = new InputData();

            string[] arrayPair = new string[2];
            arrayPair = lines[0].Split(' ');
            id.Stones = Convert.ToInt32(arrayPair[0]);
            int typeTakeStone = Convert.ToInt32(arrayPair[1]);

            string[] arrayTakes = new string[typeTakeStone];
            arrayTakes = lines[1].Split(' ');

            if (id.Stones > 0 && id.Stones <= 10000 && 
                typeTakeStone > 0 && (typeTakeStone <= id.Stones ||
                typeTakeStone <= id.Stones))
            {
                for (int i = 0; i < arrayTakes.Length; i++)
                {
                    id.TypeTakes.Add(Convert.ToInt32(arrayTakes[i]));
                }
            }
            else
            {
                ConsoleView consoleView = new ConsoleView();
                consoleView.MessageError();
            }

            return id;
        }
    }
}
