﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.TheTimeTakeStones
{
    public class InputData
    {
        public int Stones { get; set; }
        public List<int> TypeTakes { get; set; }

        public InputData()
        {
                TypeTakes = new List<int>();
        }
    }
}
