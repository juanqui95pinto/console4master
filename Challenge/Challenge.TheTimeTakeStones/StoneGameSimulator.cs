﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.TheTimeTakeStones
{
    internal class StoneGameSimulator : IStoneGameSimulator
    {
        public int PlaySimulation(InputData inData)
        {
            int count = 0;
            int remainingAmount = inData.Stones;
            bool flag = true;

            while (flag)
            {
                if (remainingAmount >= inData.TypeTakes.Min())
                {
                    count++;
                    remainingAmount -= BestTakeStoneByPlayer(remainingAmount, inData, count);
                }
                else
                    flag = false;
                if (remainingAmount < inData.TypeTakes.Min() && count % 2 == 0)
                    return 1;
            }

            return 2;
        }

        private int BestTakeStoneByPlayer(int remainingAmount, InputData inData, int count)
        {
            int index = 0;
            int takeMax = inData.TypeTakes.Max();
            int takeMin = inData.TypeTakes.Min();

            if (inData.TypeTakes.Max() == inData.Stones)
            {
                index = inData.TypeTakes.IndexOf(takeMax);
                inData.TypeTakes.RemoveAt(index);
            }

            foreach (var item in inData.TypeTakes)
            {
                if (remainingAmount >= (item + takeMin) && remainingAmount < (item + takeMin * 2))
                {
                    return item;
                }
            }

            if (remainingAmount >= takeMin && remainingAmount < (takeMin * 2))
            {
                return takeMin;
            }

            if (remainingAmount >= takeMin * 2)
            {
                return SelectBestOption(remainingAmount, inData.TypeTakes, count);
            }

            //while (rndResutl > remainingAmount) 
            //{
            //    rndResutl = RandomReult(inData.TypeTakes);
            //    if (rndResutl < remainingAmount)
            //    {
            //        return rndResutl;
            //    }
            //}

            return takeMin;
        }

        private int SelectBestOption(int remainingAmount, List<int> typeTakes, int count)
        {
            Dictionary<int,int> quotients = new Dictionary<int, int>();
            typeTakes.Reverse();

            foreach (var item in typeTakes)
            {
                if (item > 1)
                {
                    quotients.Add(item, remainingAmount / item);
                }
            }

            var sortedByKeyDesc = quotients.OrderByDescending(kv => kv.Key).ToList();

            foreach (var item in sortedByKeyDesc)
            {
                if (item.Value % 2 == 0 && count % 2 != 0)
                {
                    return item.Key;
                }
                else if (item.Value % 2 != 0 && count % 2 == 0)
                {
                    return item.Key;
                }
            }

            return typeTakes.Min();
        }

        //private int RandomReult(List<int> typeTakes)
        //{
        //    Random rnd = new Random();
        //    int rndIndex = rnd.Next(0, typeTakes.Count);
        //    return typeTakes[rndIndex];
        //}
    }
}
