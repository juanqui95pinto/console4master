﻿namespace Challenge.TheTimeTakeStones
{
    internal class Program
    {
        static void Main(string[] args)
        {
            InputData inputData = new InputData();
            DataLoader dl = new DataLoader();
            inputData =  dl.ReadFromFile();
            ConsoleView consoleView = new ConsoleView();
            //consoleView.ShowInputData(inputData);
            StoneGameSimulator sgs = new StoneGameSimulator();
            var result = sgs.PlaySimulation(inputData);
            Console.WriteLine(result);

        }
    }
}