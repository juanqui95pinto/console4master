﻿namespace Challenge.Metro
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //int[][] square = new int[][]
            //{
            //    new int[] { 0, 0, 0, 0, 0, 0 },
            //    new int[] { 0, 0, 0, 0, 0, 0 },
            //    new int[] { 0, 0, 0, 0, 0, 0 },
            //    new int[] { 0, 0, 0, 0, 0, 0 }
            //};

            //List<DiagoSquare> diagonals = new List<DiagoSquare>
            //{
            //    new DiagoSquare { X = 1, Y = 1 },
            //    new DiagoSquare { X = 2, Y = 2 },
            //    new DiagoSquare { X = 5, Y = 3 },
            //    new DiagoSquare { X = 6, Y = 4 },
            //    new DiagoSquare { X = 1, Y = 4 },
            //    new DiagoSquare { X = 1, Y = 3 }
            //};

            int[][] square = new int[][]
            {
                new int[] { 0, 0, 1 },
                new int[] { 1, 0, 0 }
            };

            List<DiagoSquare> diagonals = new List<DiagoSquare>
            {
                //new DiagoSquare { X = 0, Y = 1 },//1,1
                //new DiagoSquare { X = 2, Y = 2 },
                //new DiagoSquare { X = 5, Y = 3 },
                //new DiagoSquare { X = 6, Y = 4 },
                //new DiagoSquare { X = 1, Y = 4 },
                //new DiagoSquare { X = 1, Y = 3 }
            };

            Router router = new Router();
            var result = router.ShortPath(square, diagonals);

            Console.WriteLine(  result);
        }
    }
}