﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Metro
{
    internal interface IRouter
    {
        public int ShortPath(int[][] squareSize, List<DiagoSquare> diagonals);
    }
}
