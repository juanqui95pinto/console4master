﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Metro
{
    internal class Router : IRouter
    {
        public int ShortPath(int[][] squares, List<DiagoSquare> diagonals)
        {
            int result = 0;
            int count = 0;

            //for (int y = squareSize.Length; y > 0  ; y--)
            for (int y = 0; y < squares.Length; y++)
            {
                for (int x = 0; x < squares[0].Length; x++)
                {
                    //if (IsPartOfDiagonals(diagonals, x, y) && y <= x)
                    if (squares[y][x] == 1)//  && y >= x)
                    {
                        count++;
                        break;
                    }
                }
                //if (count > result)
                //{
                //    result = count;
                //    count = 0;
                //}
            }

            //int lines = ((squares.Length + squares[0].Length) - result) * 100;
            //int diags =  Convert.ToInt32(result * (Math.Sqrt(20000)));

            int lines = ((squares.Length + squares[0].Length) - count) * 100;
            int diags = Convert.ToInt32(count * (Math.Sqrt(20000)));

            return lines + diags;
        }

        private bool IsDiagonal(List<DiagoSquare> diagonals, int x, int y)
        {
            DiagoSquare diagoSquare = new DiagoSquare();
            diagoSquare.X = x; diagoSquare.Y = y;

            return diagonals.Contains(diagoSquare);
        }

        //private bool IsPartOfDiagonals(List<DiagoSquare> diagonals, int x, int y)
        //{
        //    DiagoSquare diagoSquare = new DiagoSquare();
        //    diagoSquare.X = x; diagoSquare.Y = y;

        //    return diagonals.Contains(diagoSquare);
        //}
    }
}
