﻿using System;
using System.Collections.Generic;

namespace ConsoleTree.binary
{
    class Program
    {
        static void Main(string[] args)
        {
            //BinaryTree<int> myBinaryTree = new BinaryTree<int>(10);

            // Create an n-ary tree with integers as data
            NaryTree<int> tree = new NaryTree<int>(1);
            TreeNode<int> rootNode = tree.Root;

            // Add children to the root node
            rootNode.AddChild(2);
            rootNode.AddChild(3);
            rootNode.AddChild(4);

            // Add children to the second child node
            rootNode.Children[1].AddChild(5);
            rootNode.Children[1].AddChild(6);

            // Print the tree structure
            Console.WriteLine("N-ary Tree:");
            PrintTree(rootNode, 0);
        }

        static void PrintTree(TreeNode<int> node, int depth)
        {
            // Print the current node
            Console.WriteLine($"{new string('-', depth)} {node.Data}");

            // Recursively print the children
            foreach (var child in node.Children)
            {
                PrintTree(child, depth + 1);
            }
        }
    }
}
