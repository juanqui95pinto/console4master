﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTree.binary
{
    class BinaryTree<T> : IEnumerable<T> where T : IComparable
    {
        public T Value { get; set; }
        public BinaryTree<T> Left { get; set; }
        public BinaryTree<T> Right { get; set; }

        public BinaryTree(T value)
        {
            Value = value;
        }

        public void Add(T newValue)
        {
            AddToNode(this, newValue);
        }
        private void AddToNode(BinaryTree<T> tree, T newValue)
        {
            if (tree.Value.CompareTo(newValue) < 0)
            {
                if (tree.Right == null)
                {
                    tree.Right = new BinaryTree<T>(newValue);
                }
                else
                {
                    AddToNode(tree.Right, newValue);
                }
            }
            if (tree.Value.CompareTo(newValue) > 0)
            {
                if (tree.Left == null)
                {
                    tree.Left = new BinaryTree<T>(newValue);
                }
                else
                {
                    AddToNode(tree.Left, newValue);
                }
            }
        }
       

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
