﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTree.binary
{
    public class NaryTree<T>
    {
        public TreeNode<T> Root { get; set; }

        public NaryTree(T rootData)
        {
            Root = new TreeNode<T>(rootData);
        }
    }
}
