﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.RabbitHunt
{
    class DataLoader : IDataLoader
    {
        public List<Rabbit> ReadFromFile()
        {
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\SampleInput.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\ThreeRabbits-basicTest.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\SevenRabbits.txt");
            string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\FiveRabbits-BasicTest.txt");

            List<Rabbit> rabits = new List<Rabbit>();
            ConsoleView consoleView = new ConsoleView();

            int amountOfRAbbits = int.Parse(lines[0]);

            if (amountOfRAbbits < 3 && amountOfRAbbits > 200)
            {
                consoleView.MessageError();
            }

            string[] arrayPair = new string[2];

            for (int i = 1; i <= amountOfRAbbits; i++)
            {
                arrayPair = lines[i].Split(' ');
                Rabbit r = new Rabbit();
                r.X = Convert.ToInt32(arrayPair[0]);
                r.Y = Convert.ToInt32(arrayPair[1]);
                rabits.Add(r);
            }

            return rabits;
        }
    }
}
