﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.RabbitHunt
{
    class Combinator : ICombinator
    {
        public List<PairRabbits> CombinationsOfRabbits(List<Rabbit> rabbits)
        {
            List<PairRabbits> combinationsOfRabbits = new List<PairRabbits>();

            for (int i = 0; i < rabbits.Count - 1; i++)
            {
                for (int j = i + 1; j < rabbits.Count; j++)
                {
                    PairRabbits pr = new PairRabbits();
                    pr.inX = rabbits[i];
                    pr.inY = rabbits[j];
                    combinationsOfRabbits.Add(pr);
                }
            }

            return combinationsOfRabbits;
        }
    }
}
