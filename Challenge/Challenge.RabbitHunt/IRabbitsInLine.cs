﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.RabbitHunt
{
    interface IRabbitsInLine
    {
        public int Calculate(List<PairRabbits> pRabbits, List<Rabbit> rabbits);
    }
}
