﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.RabbitHunt
{
    class RabbitsInLine : IRabbitsInLine
    {
        public int Calculate(List<PairRabbits> pRabbits, List<Rabbit> rabbits)
        {
            List<int> counts = new List<int>();

            EquationOfLine line = new EquationOfLine();
            foreach (var item in pRabbits)
            {
                var value = line.Equation(item, rabbits);
                counts.Add(value);
            }

            return counts.Max();
        }
    }
}
