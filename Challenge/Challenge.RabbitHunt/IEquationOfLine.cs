﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.RabbitHunt
{
    interface IEquationOfLine
    {
        public int SlopeM(PairRabbits pRabbits);
        public int IntercectionB(PairRabbits pRabbits);
        public int Equation(PairRabbits pRabbits, List<Rabbit> rabbits);
    }
}
