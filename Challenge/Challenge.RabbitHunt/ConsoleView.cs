﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.RabbitHunt
{
    class ConsoleView
    {
        public void ShowCombinations(List<PairRabbits> rabbits)
        {
            int index = 0;
            foreach (var item in rabbits)
            {
                Console.WriteLine(index++ +" "+ item.inX.X +" "+ item.inX.Y +" - "+ 
                                                item.inY.X +" "+ item.inY.Y);
            }
        }
        public void ShowRabbits(List<Rabbit> rabbits)
        {
            foreach (var item in rabbits)
            {
                Console.WriteLine(item.X +" "+ item.Y);
            }
        }
        public void MessageError()
        {
            Console.WriteLine("Number out of range!!");
        }
    }
}
