﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.RabbitHunt
{
    class EquationOfLine : IEquationOfLine
    {
        public int Equation(PairRabbits pRabbits, List<Rabbit> rabbits)
        {
            //y = m*x+b
            int m = SlopeM(pRabbits);
            int b = IntercectionB(pRabbits);
            int count = 0;

            foreach (var item in rabbits)
            {
                if (item.Y == (m * item.X + b))
                {
                    count++;
                }
            }

            return count;
        }

        public int IntercectionB(PairRabbits pRabbits)
        {
            int m = SlopeM(pRabbits);

            return pRabbits.inX.Y - (m * pRabbits.inX.X); ;
        }

        public int SlopeM(PairRabbits pRabbits)
        {
            int result = 0;

            if (pRabbits.inX.X - pRabbits.inY.X  != 0)
            {
                result = (pRabbits.inX.Y - pRabbits.inY.Y) / (pRabbits.inX.X - pRabbits.inY.X);                
            }
            else if(pRabbits.inX.X - pRabbits.inY.X == 0)
            {
                result = pRabbits.inX.Y - pRabbits.inY.Y;
            }

            return result;
        }
    }
}
