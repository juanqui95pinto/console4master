﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.RabbitHunt
{
    interface ICombinator
    {
        public List<PairRabbits> CombinationsOfRabbits(List<Rabbit> rabbits);
    }
}
