﻿using System;
using System.Collections.Generic;

namespace Challenge.RabbitHunt
{
    class Program
    {
        static void Main(string[] args)
        {
            DataLoader dataLoader = new DataLoader();
            ConsoleView consoleView = new ConsoleView();
            consoleView.ShowRabbits(dataLoader.ReadFromFile());

            Combinator combinator = new Combinator();
            //consoleView.ShowCombinations(combinator.CombinationsOfRabbits(dataLoader.ReadFromFile()));
            
            List<PairRabbits> pRabbits = combinator.CombinationsOfRabbits(dataLoader.ReadFromFile());
            List<Rabbit> rabbits = dataLoader.ReadFromFile();

            RabbitsInLine rabbitsInLine = new RabbitsInLine();
            Console.WriteLine(rabbitsInLine.Calculate(pRabbits, rabbits));
        }
    }
}
