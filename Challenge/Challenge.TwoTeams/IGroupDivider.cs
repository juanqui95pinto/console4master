﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.TwoTeams
{
    interface IGroupDivider
    {
        public List<int> BuildTeamA(Dictionary<int, List<int>> gop);
    }
}
