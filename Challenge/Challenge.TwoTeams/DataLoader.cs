﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.TwoTeams
{
    public class DataLoader : IDataLoader
    {
        public Dictionary<int, List<int>> ReadFromFile()
        {
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\input-example.txt");
            string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\best-example.txt");

            Dictionary<int, List<int>> groupOfPeople = new Dictionary<int, List<int>>();
            ConsoleView consoleView = new ConsoleView();

            int amountOfPeple = int.Parse(lines[0]);

            if (amountOfPeple <= 0)
            {
                consoleView.MessageError();
            }

            List<string> temporal = new List<string>();

            for (int i = 1; i <= amountOfPeple; i++)
            {
                temporal.Add(lines[i]);
                AddData(groupOfPeople, i, lines[i]);
            }

            return groupOfPeople;
        }

        private void AddData(Dictionary<int, List<int>> gop, int num, string chain)
        {
            gop.Add(num, ChainToNumber(chain));
        }

        private List<int> ChainToNumber(string chain)
        {
            List<int> result = new List<int>();

            string[] chainPeople = chain.Split(' ');
            result = chainPeople.Select(int.Parse)
                                .Where(x => x != 0)
                               // .Where(x => x < 0).MessageError()
                                .ToList();
            return result;
        }
    }
}
