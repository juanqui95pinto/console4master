﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.TwoTeams
{
    public class GroupDivider : IGroupDivider
    {
        public List<int> BuildTeamA(Dictionary<int, List<int>> groupOfPeople)
        {
            List<int> teamA = new List<int>();
            List<int> teamB = new List<int>();

            foreach (var item in groupOfPeople)
            {
                if (ExistsInTeam(teamA, item.Key) || ExistsInTeam(teamB, item.Key))
                {
                }
                else if (HasAFriend(groupOfPeople, teamA, item.Key))
                {
                    teamB.Add(item.Key);
                }
                else if (HasAFriend(groupOfPeople, teamB, item.Key))
                {
                    teamA.Add(item.Key);
                }
                else
                {
                    teamB.Add(item.Key);
                }
            }

            return teamA;
        }

        private bool HasAFriend(Dictionary<int, List<int>> groupOfPeople, List<int> teamA, int key)
        {
            List<int> friends = new List<int>();

            //foreach (var item in groupOfPeople)
            //{
            //    if (item.Key == key)
            //    {
            //        friends = item.Value;
            //    }
            //}

            if (groupOfPeople.ContainsKey(key))
            {
                friends = groupOfPeople[key];
            }

            foreach (var item in friends)
            {
                if (ExistsInTeam(teamA, item))
                {
                    return true;
                }
            }

            return false;
        }

        private bool ExistsInTeam(List<int> team, int key)
        {
            return team.Contains(key);
        }
    }
}
