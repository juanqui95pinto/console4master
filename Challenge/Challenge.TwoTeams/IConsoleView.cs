﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.TwoTeams
{
    interface IConsoleView
    {
        public void Show(List<int> data);
    }
}
