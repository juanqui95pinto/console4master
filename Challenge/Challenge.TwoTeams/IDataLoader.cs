﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.TwoTeams
{
    public interface IDataLoader
    {
        public Dictionary<int,List<int>> ReadFromFile();
    }
}
