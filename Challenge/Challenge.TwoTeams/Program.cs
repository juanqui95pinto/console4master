﻿namespace Challenge.TwoTeams
{
    class Program
    {
        static void Main(string[] args)
        {
            DataLoader dataLoader = new DataLoader();
            var groupOfPeople = dataLoader.ReadFromFile();
            GroupDivider groupDivider = new GroupDivider();
            var result = groupDivider.BuildTeamA(groupOfPeople);
            ConsoleView consoleView = new ConsoleView();
            consoleView.Show(result);
        }
    }
}
