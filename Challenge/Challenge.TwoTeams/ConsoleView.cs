﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.TwoTeams
{
    class ConsoleView : IConsoleView
    {
        public void Show(List<int> data)
        {
            foreach (var item in data)
            {
                Console.Write(item + " ");
            }
        }

        public void MessageError()
        {
            Console.WriteLine("invalid number!!!");
        }
    }
}
