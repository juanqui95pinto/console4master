﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Cover
{
    internal class ConsoleView
    {
        public void ErrorValidation()
        {
            Console.WriteLine("Error de entrada");
        }

        internal void ShowListOfSegments(List<Segment> segments)
        {
            foreach (var item in segments)
            {
                Console.WriteLine(item.Begin +" "+ item.End);
            }
        }
    }
}
