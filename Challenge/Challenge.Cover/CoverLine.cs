﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Cover
{
    internal class CoverLine : ICoverLine
    {
        public List<Segment> BuildCover(List<Segment> segments)
        {
            var result = new List<Segment>();
            var listSegments = segments.OrderBy(segments => segments.Begin).ThenBy(segments => segments.End).ToList();

            for (int i = 0; i < listSegments.Count; i++)
            {
                List<Segment> listResult = new List<Segment>();

                for (int j = i; j < listSegments.Count; j++)
                {
                    if (listResult.Count == 0)
                    {
                        listResult.Add(listSegments[j]);
                    }
                    else if (listSegments[j].Begin >= listResult[listResult.Count -1].End)
                    {
                        listResult.Add(listSegments[j]);
                    }
                }

                if (listResult.Count > result.Count)
                {
                    result = listResult;
                }
            }

            return result;
        }
    }
}
