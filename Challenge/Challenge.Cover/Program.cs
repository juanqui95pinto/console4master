﻿namespace Challenge.Cover
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DataLoader loader = new DataLoader();
            var inputData = loader.ReadFromFile();
           // List<Segment> listSegmentos = AscOrder(inputData);
            CoverLine coverLine = new CoverLine();
            var result = coverLine.BuildCover(inputData);
            ConsoleView consoleView = new ConsoleView();
            consoleView.ShowListOfSegments(result);

        }
    }
}