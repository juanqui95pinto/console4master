﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Cover
{
    public interface IDataLoader
    {
        public List<Segment> ReadFromFile();
    }
}
