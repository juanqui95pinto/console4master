﻿







Algoritmo
=========

- Leer los datos de entrada del archivo.txt 
  //son segmentos de linea q tienen: num-inicio y num-fin
- Guardar en la listSegmentos
- Ordenar ascendentemente listSegmentos (menor a mayor)
- listRespuesta<segmento>: contiene segmento(s) //declaracion
- listDeListas<listRespuesta>: contiene listRespuesta(s) //declaracion

* Inicio, recorre listSegmentos.Count //i
*    Inicio, recorre listSegmentos.Count //j = i 
-       Si listRespuesta esta vacia
-          Addicionar listSegmentos[j] a listRespuesta
-       Si No
-          Si listSegmentos[j].Begin >= listRespuesta[listRespuesta.Count - 1].End //q no se solape
-             Addicionar listSegmentos[j] a listRespuesta
*    Fin // Recorrer asi hasta terminar la lista de segmentos
-    Addiciona listRespuesta a listDeListas
* Fin 
- Elegir listRespuesta mas grande de listDeListas //con mas elementos
- Mostar la lista-respuesta: tamanio de lista y sus segmentos

8
1 3 i 
1 5    
1 6   
2 5   
2 6   
3 5   
3 7   
5 8   j

 1 [(1 3)(3 5)(5 8)]
 2
 3
 4
 5
 6
 7
 8
 9
 10
 11
 12



12  //listSegmentos
  0  20  f
  1   2  f
  1   5  f
  1   6  f
  2   4  f
  4   6  f
  5  10  f
 10  50  f
 30 100  f
 60 120  f
 70 130  f
110 140  f

 1 [()()()()()()()()()]
 2 [()()()()()()()()()]
 3 [()()()()()()()()()]
 4 [()()()()()()()()()]
 5 [()()()()()()()()()]
 6 [()()()()()()()()()]
 7 [()()()()()()()()()]
 8 [()()()()()()()()()]
 9 [()()()()()()()()()]
10 [()()()()()()()()()]
11 [()()()()()()()()()]
12 [()()()()()()()()()]















Algoritmo
=========

- Leer los datos de entrada del archivo.txt que son puntos en una 
  linea recta de numeros con varios segmentos q tienen un inicio 
  y un fin numerico
- Guardar en la lista<> de segmentos
- Ordenar ascendentemente esta lista de segmentos (menor a mayor)
  //Segmento es una clase q tiene: inicio y fin de tipo int y un Bool (segmento usado:True)
  //lista-respuesta: contendra segmentos de un recorrido

  Inicio // llenado de lista-respuesta
-     Recorrer la lista<> de segmentos
-     Tomar el i-segmento (i-inicio y i-fin), y addicionar en la lista-respuesta///////////////
-     Si existe varios segmentos con mismo i-inicio: de ellos se elige el mas pequenio
      //de ellos se elige el segmento de menor longitud
-     Tomar el segmento cuyo inicio sea mayor igual al final del i-segmento 
      (el siguiente mas cercano - q no se solape)
-     Aniadir a lista-repuesta ///////////////////////////////
-     Si existe varios segmentos con mismo i-inicio: de ellos se elige el mas pequenio
      //de ellos se elige el segmento de menor longitud

-     Recorrer asi hasta terminar la lista de segmentos
  Fin // llenado de lista-respuesta
- mostar la lista-respuesta: tamanio de lista y sus segmentos



12
  0  20  f
  1   2  f
  1   5  f
  1   6  f
  2   4  f
  4   6  f
  5  10  f
 10  50  f
 30 100  f
 60 120  f
 70 130  f
110 140  f

[(0,20)(30,100)(110,140)]          3
[(1,2)(2,4)(4,6)(10,50)(60,120)]   5 <--
[(1,5)(5,10)(10,50)(60,120)]       4
[(1,6)(10,50)(60,120)]             3
[(2,4)(4,6)(10,50)(60,120)]        4
[(4,6)(10,50)(60,120)]             3

Algoritmo
=========

- Leer los datos de entrada del archivo.txt 
  son segmentos de linea q tienen: num-inicio y num-fin
- Guardar en la listSegmentos
- Ordenar ascendentemente esta lista de segmentos (menor a mayor)
  //Segmento, clase tiene propiedades: inicio y fin
- listDeListas: contiene varios listRespuesta //declaracion
  Inicio // p llenar listDeListas            /////////////////////////////

     //listRespuesta: contiene segmentos elegidos //declaracion
-    Inicio //p llenar listRespuesta con segmentos
        Si listRespuesta esta vacia
-          Tomar el i-segmento (i-inicio y i-fin), 
           Si existe varios segmentos con mismo i-inicio: de ellos se elige el mas pequenio
           //de ellos se elige el segmento de menor longitud
-          Addicionar segmento a listRespuesta
        Si No
-          Tomar el segmento cuyo inicio sea mayor igual al final del i-segmento 
           (el siguiente mas cercano - "q no se solape")
           Si existe varios segmentos con mismo i-inicio: de ellos se elige el mas pequenio
           //de ellos se elige el segmento de menor longitud
-          Addicionar segmento a listRespuesta
-       Recorrer asi hasta terminar la lista de segmentos
     Fin // llenado de lista-respuesta
     Addiciona listRespuesta a listDeListas
  Fin 
- Elegir listRespuesta mas grande //con mas elementos
- Mostar la lista-respuesta: tamanio de lista y sus segmentos

12
  0  20  
  1   2   
  1   5   //
  1   6  
  2   4  
  4   6  
  5  10  
 10  50  
 30 100  
 60 120  
 70 130  
110 140 

[(0,20)()()()()()()()]

[()()()()()()()()]
[()()()()()()()()]



6
 -140 -110  
 -130  -70  
 -120  -60  
 -100  -30   
  -50  -10  
  -20    0  

 1 [(-140,-110)()()()]
 2 [()()()()]
 3 [()()()()]
 4 [()()()()]
 5 [()()()()]
 6 [()()()()]
---> [(-140,-110)(-100,-30)(-20,0)]
---> [(-140,-110)(-100,-30)(-20,0)]  3

6
1  5  
2  6   
3  7  
4  8  
5  9  
6 10

1 [(1,5)(5,9)]  2  <--
2 [(2,6)(6 10)] 2
3 [(3,7)]       1
  [(4,8)]       1
  [(5,9)]       1
  [(6,10)]      1

12
  0  20  
  1   2   
  1   5   //
  1   6  
  2   4  
  4   6  
  5  10  
 10  50  
 30 100  
 60 120  
 70 130  
110 140  

 1 [(0,20)(30,100)(110,140)]         3
 2 [(1,2)(2,4)(4,6)(10,50)(60,120)]  5 <--
 3 [(1,5)(5,10)(10,50)(60,120)]      4
 4 [(1,6)(10,50)(60,120)]            3
 5 [(2,4)(4,6)(10,50)(60,120)]       4
 6 [(4,6)(10,50)(60,120)]            3
 7 [(5,10)(10,50)(60,120)]           3
 8 [(10,50)(60,120)]                 2
 9 [(30,100)(110,140)]               2
10 [(60,120)]                        1
11 [(70,130)]                        1
12 [(110,140)]                       1



3
1 5  t
3 5  f
5 6  t
---> [(1,5)(5,6)]  2

6
1  5  t
2  6  f  
3  7  f
4  8  f
5  9  t
6 10  f
---> [(1,5)(5,9)]  2

6
 -140 -110  t
 -130  -70  f
 -120  -60  f
 -100  -30  t 
  -50  -10  f
  -20    0  t
---> [(-140,-110)(-100,-30)(-20,0)]
---> [(-140,-110)(-100,-30)(-20,0)]  3

6
  0  20  t
 10  50  f
 30 100  t
 60 120  f
 70 130  f
110 140  t
--->  [(0,20)(30,100)(110,140)]  3
--->  [(0,20)(30,100)(110,140)]  3

3
1 3  t
2 5  f
3 6  t
--> [(1,3)(3,6)]   2

6
1 5    t
1 20   f
5 10   t
10 15  t
15 20  t
20 25  f
--> [(1,5)(5,10)(10,15)(15,20)]   4


Algoritmo
=========

- Leer los datos de entrada del archivo.txt que son puntos en una 
  linea recta de numeros con varios segmentos q tienen un inicio 
  y un fin numerico

- Guardar en la lista<> de segmentos
- ordenar ascendentemente esta lista de segmentos
  //Segmento es una clase q tiene: inicio y fin ambos de tipo entero
  //y un bool usado q puede ser true o false  
  lista de lista-respuestas: contendra varias lista-respuestas
  Inicio //llenado de lista de lista-respuestas
    lista-respuesta: contendra segmentos de un recorrido
    Inicio // llenado de lista-respuesta
-     Recorrer la lista<> de segmentos
      Tomar el i-segmento (i-inicio y i-fin), y addicionarle en la lista de lista-respuesta
-     si existiese varios con mismo i-inicio: de ellos se elige el mas pequenio
      //de ellos se elige el segmento de menor longitud
      tomar el segmento cuyo inicio sea mayor igual al final del i-segmento // otro ciclo
      y aniadirlo a lista-repuesta
-     si existiese varios con mismo i-inicio: de ellos se elige el mas pequenio
      //de ellos se elige el segmento de menor longitud
-     recorrer asi hasta terminar la lista de segmentos
    Fin // llenado de lista-respuesta
-   adicionar lista-respuesta a lista de lista-respuestas
  Fin //llenado de lista de lista-respuestas
- de la lista de lista-respuesta seleccionar la lista-respuesta con mayor tamanio

- mostar la lista-respuesta: tamanio de lista y sus segmentos


Nota: 
toma el segmento con el menor inicio la 1ra vez 
luego de armar la 1ra lista-respuesta, 
tomar otra vez el mismo 1ro de la 1ra lista-respuesta, 
y buscar otro camino q no sea los siguiente de la 1ra lista-respuesta,
asi hasta terminar las posibilidades
usando siempre el mismo 1er segmento 
y cambiar el q sigue, de acuerdo a eso continuar algoritmo

si existe otro segmento q inicie igual al primero de la 1ra lista-respuesta, 
tomar ese hasta armar una lista-respuesta,
luego tomando ese inicial cambiar el siguiente y en base a ese
continuar con el algoritmo

asi hasta terminar los Primeros
una vez terminados los Primeros
tomar como primero al siguiente segmento con el menor inicio
y seguir el algoritmo en base a ese segmento tomado



- Elije el segmento con el menor inicio la 1ra vez
  se continua creando la lista-respuesta (segun algoritmo)

- Elije el segmento con el menor inicio (el mismo de la 1ra vez)
  se continua creando la lista-respuesta (pero eligiendo otro 2do)
  asi hasta terminar los caminos alternativos o posibilidades
  //usando siempre el mismo 1er segmento
- 
//si hay mas de uno q inicia con el mismo numero 

--------------------
6
1 5
1 20
5 10
10 15
15 20
20 25

5            2
1 5           
5 10         1 20   
10 15       20 25 
15 20
20 25
-------------------
3
3 6
1 3
2 5
-->
2
1 3
3 6

--------------------
6
  0  20
 10  50
 30 100
 60 120
 70 130
110 140

--->
3
  0  20
 30 100
110 140

--------------------

6
  -20    0
  -50  -10
 -100  -30 
 -120  -60 
 -130  -70
 -140 -110

2
-140 -110
 -30 -100
 -20    0
--------------------
6
1  5
2  6
3  7
4  8
5  9
6 10

2
1 5
5 9
--------------------
3
3 5
1 5
5 6

2
1 5
5 6

