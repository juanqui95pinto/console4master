﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Cover
{
    internal interface ICoverLine
    {
        public List<Segment> BuildCover(List<Segment> segments);
    }
}
