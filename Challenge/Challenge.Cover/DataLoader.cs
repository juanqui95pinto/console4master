﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Cover
{
    public class DataLoader : IDataLoader
    {
        public List<Segment> ReadFromFile()
        {
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\OriginalExample.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\EightExample.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\SixPositive.txt");
            string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\TwelveExample.txt");

            List<Segment> segments = new List<Segment>();
            int amountOfSegments = int.Parse(lines[0]);
            ConsoleView consoleView = new ConsoleView();

            if (amountOfSegments >= 1 && amountOfSegments <= 99)
            {
                string[] arrayPair = new string[2];

                for (int i = 1; i <= amountOfSegments; i++)
                {
                    arrayPair = lines[i].Split(' ');
                    Segment segment = new Segment();
                    segment.Begin = Convert.ToInt32(arrayPair[0]);
                    segment.End = Convert.ToInt32(arrayPair[1]);
                    segments.Add(segment);
                }
                //consoleView.ShowDataInput(segments);
            }
            else
            {
                consoleView.ErrorValidation();
            }

            return segments;
        }

    }
}
