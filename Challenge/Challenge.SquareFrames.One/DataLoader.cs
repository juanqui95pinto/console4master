﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareFrames.One
{
    public class DataLoader : IDataLoader
    {
        //public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\BasicSquaresExample.txt");
        //public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\ComplexSquareExample.txt");
        public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\Example.txt");

        string linea;

        public char[][] ReadFromFile()
        {
            char[][] canvasResult = new char[20][];
            int i = 0;

            while ((linea = fileTxt.ReadLine()) != null)
            {
                canvasResult[i] = linea.ToCharArray();
                i++;
            }

            return canvasResult;
        }
    }
}
