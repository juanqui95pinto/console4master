﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareFrames.One
{
    public class SequenceBuilder : ISequenceBuilder
    {
        public SquareSequence BuildSquareSequence(SquarePicture canvasSquare)
        {
            SquareSequence squareSequence = new SquareSequence();

            while (true)
            {
                var squares = IdentifySquaresInTopLevel(canvasSquare);

                if (squares.Count == 0)
                {
                    break;
                }
            }

            return squareSequence;
        }

        private static List<Square> IdentifySquaresInTopLevel(SquarePicture canvasSquare)
        {
            ConsoleSecuenceVisualizer consoleSecuenceVisualizer = new ConsoleSecuenceVisualizer();
            var result = new List<Square>();
            Tracker tracker = new Tracker();
            SquarePicture squarePicture = new SquarePicture(canvasSquare.canvas);

            for (int y = 0; y < canvasSquare.canvas.Length; y++)
            {
                for (int x = 0; x < canvasSquare.canvas[0].Length; x++)
                {
                    if (CornerFound(canvasSquare, y, x))
                    {
                        var square = tracker.TryIdentifySquare(canvasSquare, y, x);
                        if (square != null)
                        {
                            squarePicture.SidesToAsterisk(square);
                            consoleSecuenceVisualizer.VisualizeMatrix(canvasSquare.canvas);
                            //canvasSquare.Draw(square);
                            //consoleSecuenceVisualizer.VisualizeMatrix(canvasSquare.canvas);

                            result.Add(square);
                        }
                        else 
                        {
                            Square inferSquare = tracker.InferSquare(canvasSquare, y, x);
                            if (inferSquare != null)
                            {
                                canvasSquare.Draw(inferSquare);
                                consoleSecuenceVisualizer.VisualizeMatrix(canvasSquare.canvas);
                                result.Add(inferSquare);
                                squarePicture.SidesToAsterisk(inferSquare);
                                consoleSecuenceVisualizer.VisualizeMatrix(canvasSquare.canvas);
                            }
                        }

                        
                    }
                }
            }

            return result;
        }

        private static bool CornerFound(SquarePicture canvasSquare, int y, int x)
        {
            return canvasSquare.canvas[y][x] == '┌' || canvasSquare.canvas[y][x] == '┐' ||
                   canvasSquare.canvas[y][x] == '└' || canvasSquare.canvas[y][x] == '┘';
        }
    }
}
