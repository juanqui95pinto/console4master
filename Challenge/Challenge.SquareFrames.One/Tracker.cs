﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareFrames.One
{
    class Tracker
    {
        internal Square TryIdentifySquare(SquarePicture canvas, int y, int x)
        {
            var square = TryIdentifyOnFirstLevel(canvas, y, x);

            if (square != null) return square;



            return null;
        }

        private Square TryIdentifyOnFirstLevel(SquarePicture canvas, int y, int x)
        {
            Square square = new Square();
            int side = SizeUpperSideOfSquare(canvas.canvas, y, x);
            int one = 1;
            int two = 2;
            bool sidesOfAquare = false;

            if (TheyAreCorners(canvas.canvas, y, x, side))
            {
                if (side == two)
                {
                    square.X = x;
                    square.Y = y;
                    square.Size = side;
                    square.Level = one;

                    return square;
                }

                for (int i = one; i <= side - two; i++)
                {
                    if (canvas.canvas[y][x + i] == '─' && canvas.canvas[y + i][x] == '│' &&
                        canvas.canvas[y + side - one][x + i] == '─' && canvas.canvas[y + i][x + side - one] == '│')
                    {
                        sidesOfAquare = true;
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            if (sidesOfAquare)
            {
                square.X = x;
                square.Y = y;
                square.Size = side;
                square.Level = one;

                return square;
            }

            return null;
        }

        private bool TheyAreCorners(char[][] canvas, int y, int x, int side)
        {
            int one = 0;
            if (side > 0)
            {
                one = 1;
            }
            if (canvas[y][x] == '┌' && canvas[y][x + side - one] == '┐' &&
                canvas[y + side - one][x] == '└' && canvas[y + side - one][x + side - one] == '┘')
            {
                return true;
            }

            return false;
        }

        internal Square InferSquare(SquarePicture canvasSquare, int y, int x)
        {
            //var canvas = canvasSquare.canvas;
            //Square square = new Square();
            //int two = 2;
            //int one = 1;

            //if (canvas[y][x] == '┌' && canvas[y][x + 1] == '*' && 
            //    canvas[y + 1][x] == '*' && canvas[y + 1][x + 1] == '*')
            //{
            //    square.X = x;
            //    square.Y = y;
            //    square.Size = two;
            //    square.Level = one;

            //    return square;
            //}
            //else if (FoundLitleSquare(canvas, y, x) != null)
            //{
            //    square = FoundLitleSquare(canvas, y, x);

            //    return square;
            //}

            //if (canvas[y][x] == '┌')
            //{
            //    int sideX = CheckLeftUpperToRight(canvas, y, x);
            //    int sideY = CheckLeftUpperToBottom(canvas, y, x);

            //    int side = SelectedSide(sideY, sideX);

            //    if (CalculateSquare(canvas, y, x, side) != null)
            //    {
            //        return CalculateSquare(canvas, y, x, side);
            //    }
            //}

            return null;
        }

        private Square CalculateSquare(char[][] canvas, int y, int x, int side)
        {
            int one = 1;
            int two = 2;
            Square square = new Square();

            if (canvas[y][x + side - 1] == '*' && canvas[y + side - 1][x] == '*'
                && canvas[y + side - 1][x + side - 1] == '*')
            {
                for (int i = one; i <= side - two; i++)
                {
                    if ((canvas[y + i][x] == '│' || canvas[y + i][x] == '*') &&
                        (canvas[y][x + i] == '─' || canvas[y][x + i] == '*'))
                    {
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            square.X = x;
            square.Y = y;
            square.Size = side;
            square.Level = one;

            return square;
        }

        private int SelectedSide(int sideY, int sideX)
        {
            if (sideX == 0 && sideY > 0)
            {
                return sideY;
            }

            if (sideX > 0 && sideY == 0)
            {
                return sideX;
            }

            if (sideX > 0 && sideY > 0)
            {
                if (sideX == sideY)
                {
                    return sideX;
                }
            }

            return 0;
        }

        private int CheckLeftUpperToBottom(char[][] canvas, int y, int x)
        {
            int result = 0;
            int countY = y + 1;



            while (canvas[countY][x] == '│')
            {
                countY++;
            }

            if (countY++ == '*')
            {
                return (countY - y) + 1;
            }

            return result;
        }

        private int CheckLeftUpperToRight(char[][] canvas, int y, int x)
        {
            int result = 0;
            int countX = x + 1;

            while (canvas[y][countX] == '─')
            {
                countX++;
            }

            if (countX++ == '*')
            {
                return (countX - x) + 1;
            }

            return result;
        }

        private Square FoundLitleSquare(char[][] canvas, int y, int x)
        {
            Square square = new Square();
            int two = 2;
            int one = 1;

            if (canvas[y][x] == '┐' && canvas[y][x - 1] == '*' &&
                canvas[y + 1][x] == '*' && canvas[y + 1][x - 1] == '*')
            {
                square.X = x;
                square.Y = y;
                square.Size = two;
                square.Level = one;

                return square;
            }
            if (canvas[y][x] == '└' && canvas[y][x + 1] == '*' &&
                canvas[y - 1][x] == '*' && canvas[y - 1][x + 1] == '*')
            {
                square.X = x;
                square.Y = y;
                square.Size = two;
                square.Level = one;

                return square;
            }
            if (canvas[y][x] == '┘' && canvas[y][x - 1] == '*' &&
                canvas[y - 1][x] == '*' && canvas[y - 1][x - 1] == '*')
            {
                square.X = x;
                square.Y = y;
                square.Size = two;
                square.Level = one;

                return square;
            }
            return null;
        }

        private int SizeUpperSideOfSquare(char[][] canvas, int y, int x)
        {
            int countX = x + 1;
            int two = 2;

            if (canvas[y][x] == '┌' && canvas[y][countX] == '┐') return two;

            if (canvas[y][x] == '┌')
            {
                while (canvas[y][countX] == '─')
                {
                    countX++;
                }

                if (canvas[y][countX] == '┐')
                {
                    return (countX - x) + 1;
                }
            }

            return 0;
        }
    }
}
