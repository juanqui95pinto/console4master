﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareFrames.One
{
    public class SquarePicture
    {
        public char[][] canvas;
        public SquarePicture(char[][] canvas)
        {
            this.canvas = canvas;
        }

        public void PrintCanvaSquares()
        {
            ConsoleSecuenceVisualizer consoleSecuenceVisualizer = new ConsoleSecuenceVisualizer();
            consoleSecuenceVisualizer.VisualizeMatrix(canvas);
        }

        internal void ChangeSquareToAsteriscs(SquareSequence squareSequence)
        {
            foreach (var square in squareSequence.Squares)
            {
                SidesToAsterisk(square);
            }
        }

        public void SidesToAsterisk(Square square)
        {
            int one = 1;
            int two = 2;

            if (square.Size == two)
            {
                canvas[square.Y][square.X] = '*';
                canvas[square.Y][square.X + 1] = '*';
                canvas[square.Y + 1][square.X] = '*';
                canvas[square.Y + 1][square.X + 1] = '*';
            }

            for (int i = 0; i < square.Size; i++)
            {
                canvas[square.Y][square.X + i] = '*';
                canvas[square.Y + i][square.X] = '*';
                canvas[square.Y + square.Size - one][square.X + i] = '*';
                canvas[square.Y + i][square.X + square.Size - one] = '*';
            }
        }

        internal void Draw(Square square)
        {
            int one = 1;
            int two = 2;


            canvas[square.Y][square.X] = '┌';
            canvas[square.Y][square.X + square.Size - one] = '┐';
            canvas[square.Y + square.Size - one][square.X] = '└';
            canvas[square.Y + square.Size - one][square.X + square.Size - one] = '┘';

            for (int i = one; i <= square.Size - two; i++)
            {
                canvas[square.Y][square.X + i] = '─';
                canvas[square.Y + i][square.X] = '│';
                canvas[square.Y + square.Size - one][square.X + i] = '─';
                canvas[square.Y + i][square.X + square.Size - one] = '│';
            }
        }
        
    }
}
