﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareFrames.One
{
    class ConsoleSecuenceVisualizer:ISequenceVisualizer
    {
        public void Visualize(SquareSequence squares)
        {
            int count = 1;
            Console.WriteLine("\n\t\t" + squares.Size);
            Console.WriteLine("\n\t\tX \tY \tside");
            foreach (var item in squares.Squares)
            {
                Console.WriteLine("\t{0}:\t{1}, \t{2}, \t {3}", count, item.X, item.Y, item.Size);
                count++;
            }
        }
        public void VisualizeMatrix(char[][] matrix)
        {
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[0].Length; j++)
                {
                    Console.Write(matrix[i][j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
