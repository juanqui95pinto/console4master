﻿using System;

namespace Challenge.SquareFrames.One
{
    class Program
    {
        static void Main(string[] args)
        {
            //// ConsoleSecuenceVisualizer consoleSecuenceVisualizer = new ConsoleSecuenceVisualizer();

            //// var picProv = new PictureProvider(new DataLoader());
            //// SquarePicture canvasSquare = picProv.ProvidePicture();

            ////// canvasSquare.PrintCanvaSquares();
            //// SequenceBuilder sequence = new SequenceBuilder();
            //// var squareSequence = sequence.BuildSquareSequence(canvasSquare);
            ////// consoleSecuenceVisualizer.Visualize(squareSequence);

            //// consoleSecuenceVisualizer.VisualizeMatrix(canvasSquare.canvas);

            //var picProv2 = new PictureProvider(new DataLoader());
            //var squarePicture = picProv2.ProvidePicture ();
            //consoleSecuenceVisualizer.VisualizeMatrix(squarePicture.canvas);
            //var sq = new Square();
            //sq.Size = 2;
            //sq.X = 1;
            //sq.Y = 1;
            //squarePicture.Draw(sq);

            //consoleSecuenceVisualizer.VisualizeMatrix(squarePicture.canvas);



            //char[,] matriz = new char[,]
            //{
            //    {'┌', '┐', '.', '.', '.', '.'},
            //    {'└', '┘', '.', '.', '.', '.'},
            //    {'.', '.', '.', '.', '.', '.'},
            //    {'.', '.', '.', '.', '.', '.'}
            //};

           // char[,] matriz = new char[,]
           //{
           //     {'┌', '─', '┐', '.', '.', '.'},
           //     {'│', '.', '│', '.', '.', '.'},
           //     {'└', '─', '┘', '.', '.', '.'},
           //     {'.', '.', '.', '.', '.', '.'}
           //};

            char[,] matriz = new char[,]
            {
                {'┌', '─', '─', '┐', '.', '.'},
                {'│', '.', '.', '│', '.', '.'},
                {'│', '.', '.', '│', '.', '.'},
                {'└', '─', '─', '┘', '.', '.'}
            };
            int x = 0; // Posición X de la esquina superior izquierda
            int y = 0; // Posición Y de la esquina superior izquierda

            Cuadrado cuadrado = InferirCuadrado(x, y, matriz);

            //Console.WriteLine($"X: {cuadrado.X}, Y: {cuadrado.Y}, Ancho Arriba: {cuadrado.Ancho}, " 
            //    + $"Altura: {cuadrado.Altura}" + $"AlturaDerecha: {cuadrado.alturaDerecha}"
            //    + $"AlturaDerecha: {cuadrado.alturaDerecha}");
        }
        static Cuadrado InferirCuadrado(int x, int y, char[,] matriz)
        {
            int width = matriz.GetLength(1);
            int height = matriz.GetLength(0);

            int anchoArriba = 0, anchoAbajo = 0;
            int altura = 0, alturaDerecha = 0;

            // Encontrar el ancho arriba del cuadrado
            for (int i = x; i < matriz.GetLength(1); i++)
            {
                if (matriz[y, i] == '┌' || matriz[y, i] == '┐' || matriz[y, i] == '─')
                    anchoArriba++;
                else
                    break;
            }
            //altura lado derecho de arriba abajo
            for (int j = y; j < height; j++)
            {
                if (matriz[j, x + anchoArriba -1] == '┐' || matriz[j, x + anchoArriba - 1] == '┘' || matriz[j, x] == '│')
                    alturaDerecha++;
                else
                    break;
            }
            // Encontrar el ancho abajo del cuadrado
            for (int i = x; i < width; i++)
            {
                if (matriz[y + anchoArriba - 1, i + anchoArriba - 1] == '┘' || matriz[y + anchoArriba - 1, i + anchoArriba - 1] == '└' || matriz[y, i] == '─')
                    anchoAbajo++;
                else
                    break;
            }
            // Encontrar la altura del cuadrado
            for (int j = y; j < height; j++)
            {
                if (matriz[j, x] == '┌' || matriz[j, x] == '└' || matriz[j, x] == '│')
                    altura++;
                else
                    break;
            }

            return new Cuadrado(x, y, anchoArriba, anchoAbajo, altura, alturaDerecha);
        }
    }
    class Cuadrado
    {
        public int X { get; }
        public int Y { get; }
        public int AnchoArriba { get; }
        public int AnchoAbajo { get; }
        public int Altura { get; }
        public int alturaDerecha { get; }

        public Cuadrado(int x, int y, int anchoArriba, int anchoAbajo, int altura, int alturaDerecha)
        {
            X = x;
            Y = y;
            AnchoArriba = anchoArriba;
            AnchoAbajo = anchoAbajo;
            Altura = altura;
            this.alturaDerecha = alturaDerecha;
        }
    }
}
