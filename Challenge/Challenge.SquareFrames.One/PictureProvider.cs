﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareFrames.One
{
    class PictureProvider:IPictureProvider
    {
        IDataLoader loader;
        public PictureProvider(IDataLoader loader)
        {
            this.loader = loader;
        }
        public SquarePicture ProvidePicture()
        {
            return new SquarePicture(loader.ReadFromFile());
        }
    }
}
