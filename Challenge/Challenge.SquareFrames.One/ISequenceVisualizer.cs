﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareFrames.One
{
    interface ISequenceVisualizer
    {
        void Visualize(SquareSequence s);
    }
}
