﻿using Challenge.SquareCountry.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareCountry
{
    class BuiltResults : IBuiltResults
    {
        public int FindResult(List<int> squares, int num)
        {
            List<int> results = FillResults(squares, num);

            return results.Min();
        }


        public List<int> FillResults(List<int> squares, int num)
        {
            List<int> results = new List<int>();

            squares = CombinatorSquares(squares);
            for (int i = 0; i < squares.Count - 1; i++)
            {
                var espcialCase = DivisionCase(squares, num);
                if (espcialCase != 0 && espcialCase != num)
                {
                    results.Add(espcialCase);
                }

                var normalCase = SubstractionCase(squares, num);
                results.Add(normalCase);
            }
                

            return results;
        }

        private List<int> CombinatorSquares(List<int> squares)
        {
            List<int> newSquares = new List<int>();
            for (int i = 0; i < squares.Count - 1; i++)
            {
                for (int j = i + 1; j < squares.Count; j++)
                {
                    newSquares.Add(squares[j]);
                }
            }

            return newSquares;
        }

        private int SubstractionCase(List<int> squares, int num)
        {
            int count = 0;

            foreach (var element in squares)
            {
                if (element <= num)
                {
                    num -= element;
                    count++;
                    if (num <= 3)
                    {
                        count += num;
                        num = 0;
                    }
                    if (num == 0)
                        break;
                }
            }

            return count;
        }

        private int DivisionCase(List<int> squares, int num)
        {
            int count = 0;
            foreach (var item in squares)
            {
                if (num >= item && num % item == 0)
                {
                    count = num / item;
                    break;
                }
            }

            return count;
        }
    }
}
