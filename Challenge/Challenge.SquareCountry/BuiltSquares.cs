﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareCountry
{
    class BuiltSquares : IBuiltSquares
    {
        public List<int> Squares(int number)
        {
            List<int> squares = new List<int>();

            double rootSquare = Math.Sqrt(number);

            int rootSquareInteger = (int)Math.Round(rootSquare);

            for (int i = rootSquareInteger; i > 0; i--)
            {
                squares.Add(i*i);
            }

            return squares;
        }
    }
}
