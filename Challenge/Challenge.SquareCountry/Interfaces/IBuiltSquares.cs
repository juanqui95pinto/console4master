﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareCountry
{
    interface IBuiltSquares
    {
        public List<int> Squares(int number);
    }
}
