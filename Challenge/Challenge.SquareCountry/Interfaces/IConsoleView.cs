﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareCountry
{
    interface IConsoleView
    {
        public int InputData();
        public void ShowResult(int result);
    }
}
