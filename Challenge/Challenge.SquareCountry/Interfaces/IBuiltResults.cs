﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareCountry.Interfaces
{
    interface IBuiltResults
    {
        public int FindResult(List<int> squares, int num);
        public List<int> FillResults(List<int> squares, int num);
    }
}
