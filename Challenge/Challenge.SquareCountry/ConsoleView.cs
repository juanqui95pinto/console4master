﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.SquareCountry
{
    class ConsoleView:IConsoleView
    {
        public int InputData()
        {
            Console.Write("Input number: ");
            var result = Convert.ToInt32(Console.ReadLine());
            return result;
        }

        public void ShowResult(int result)
        {
            Console.WriteLine(result);
        }
    }
}
