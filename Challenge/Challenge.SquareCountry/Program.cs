﻿using System;
using System.Collections.Generic;

namespace Challenge.SquareCountry
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleView cview = new ConsoleView();
            int iData = cview.InputData();
            BuiltSquares sqrs = new BuiltSquares();
            List<int> squares = sqrs.Squares(iData);

            BuiltResults br = new BuiltResults();
            br.FillResults(squares, iData);
            var result = br.FindResult(squares, iData);
            cview.ShowResult(result);




        }
    }
}
