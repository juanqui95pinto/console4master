﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Stars
{
    class LevelsOfStars : ILevelsOfStars
    {
        public List<int> BuiltLevels(List<Star> stars)
        {
            int[] arrStars = new int[stars.Count];
            int countLevel = 0;

            foreach (var star in stars)
            {
                countLevel = LessStar(stars, star);
                countLevel--;
                arrStars[countLevel]++;
            }

            //for (int i = 0; i < arrStars.Length; i++)
            //    Console.WriteLine("[" + i + "]" + " " + arrStars[i]);

            return arrStars.ToList();
        }

        private int LessStar(List<Star> stars, Star star)
        {
            List<Star> lessPairs = stars.Where(s => s.X <= star.X && s.Y <= star.Y).ToList();

            return lessPairs.Count();
        }
    }
}
