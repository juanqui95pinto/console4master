﻿using System;

namespace Challenge.Stars
{
    class Program
    {
        static void Main(string[] args)
        {
            DataLoader dl = new DataLoader();
            LevelsOfStars los = new LevelsOfStars();
            var levels = los.BuiltLevels(dl.ReadFromFile());
            ConsoleView consoleView = new ConsoleView();
            consoleView.ShowResult(levels);

        }
    }
}
