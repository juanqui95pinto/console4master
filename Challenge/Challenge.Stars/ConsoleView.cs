﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Stars
{
    class ConsoleView
    {
        public void MessageError()
        {
            Console.WriteLine("Number out of range!!");
        }

        public void ShowDataInput(int amountOfStars, List<Star>  stars)
        {
            Console.WriteLine(amountOfStars);
            foreach (var item in stars)
            {
                Console.WriteLine(item.X + " " + item.Y);
            }
        }

        public void ShowResult(List<int> levels)
        {
            foreach (var item in levels)
            {
                Console.WriteLine(item);
            }
        }
    }
}
