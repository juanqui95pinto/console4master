﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Stars
{
    class DataLoader : IDataLoader
    {
        public List<Star> ReadFromFile()
        {
            string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\OriginalExample.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\LargeExample.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\DiagonalExample.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\ParalelXExample.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\ParalelYExample.txt");
            //string[] lines = File.ReadAllLines("..\\..\\..\\Resources\\SimetricExample.txt");

            List<Star> stars = new List<Star>();
            ConsoleView consoleView = new ConsoleView();
            int amountOfStars = int.Parse(lines[0]);

            if (amountOfStars >= 1 && amountOfStars <= 1500)
            {
                string[] arrayPair = new string[2];

                for (int i = 1; i <= amountOfStars; i++)
                {
                    arrayPair = lines[i].Split(' ');
                    Star s = new Star();
                    s.X = Convert.ToInt32(arrayPair[0]);
                    s.Y = Convert.ToInt32(arrayPair[1]);
                    stars.Add(s);
                }
            }
            else
            {
                consoleView.MessageError();
            }
            consoleView.ShowDataInput(amountOfStars, stars);

            return stars;
        }
    }
}
