﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Stars
{
    interface ILevelsOfStars
    {
        public List<int> BuiltLevels(List<Star> stars);
    }
}
